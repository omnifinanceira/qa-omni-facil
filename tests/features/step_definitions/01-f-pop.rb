Quando("eu acesso o novo menu") do
  home = Home.new
  home.acessar_novo_menu
  @util = Util.new
end

Quando("seleciono o {string}") do |agente|
  @proposta = SimuladorRodas.new
  @proposta.selecionar_agente(agente)
end

Entao("seleciono o {string}, {string} e {string}") do |menu, submenu, subsubmenu|
  @proposta.selecionar_menu(menu)
  @proposta.selecionar_submenu(submenu)
  @proposta.selecionar_submenu(subsubmenu)
end

# BUSCAR OS DADOS DE POP PARA INSERIR AQUI
Entao("seleciono as opcoes de simulador pop") do
  @proposta.selecionar_tipo_financiamento("CDC")
  @proposta.selecionar_tabela("70")
  @proposta.selecionar_produto("POP")
  @proposta.selecionar_tipo_credito("NORMAL")
  @proposta.selecionar_objeto_financiamento("BCO CDC-POP-B-NR-R4")
end

# VERIFICAR SE ESTE CAMPO É O MESMO PARA POP
Quando("preencho os dados do simulador do pop") do
  @proposta.preencher_dados_simulador_pop

  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
  end
end

Quando("preencho os dados da proposta de negocio do simulador pop") do
  @proposta.preencher_proposta_negocio_simulador("pop")
  @util.pegar_dados_parcela
  @proposta.clicar_preencher_proposta
end

Quando("o cpf do cliente pop") do
  sql = Queries.new
  cpf = sql.pegar_cpf
  @proposta.preencher_cpf_cliente(cpf)
end

Entao("clico no botao validar para validar as informacoes pop") do
  @proposta.clicar_botao_validar_cliente

  within_frame(find(:xpath, "//iframe")[:id]) do
    textotitulo = find(:xpath, "//tbody/tr/td/font[@class='txtAzulBold12']")
    expect(/Consulta Base Histórica/.match(textotitulo.text)).to be_truthy
  end
end

Quando("clico em proximo na consulta de base historica pop") do
  @proposta.clicar_proximo_base_historica
end

Quando("preencho os dados de operador pop") do
  @proposta.preenche_dados_operador
end

Quando("cadastro de pessoa fisica pop") do
  @proposta.preenche_dados_pessoais
end

Quando("dados profissionais pop") do
  @proposta.preenche_dados_profissionais
  @proposta.clicar_ok_alerta
end

Quando("dados do pop") do
  @proposta.preencher_dados_pop
end

Quando("dados de referencia pop") do
  @proposta.preenche_dados_referencias
end

Quando("preencho a proposta de negocios pop") do
  @proposta.preenche_proposta_de_negocios
end

Quando("clico no botao gravar pop") do
  begin
    @proposta.processar("Gravar")
  rescue
  end
end

Entao("clico no botao processar travas pop") do
  begin
    @proposta.processar("Processar Travas")
    @proposta.clicar_ok_alerta
  rescue
  end
end

Quando("acesso a pagina de consulta historica pop") do
  @proposta.nova_tela
end

Quando("clico no botao aceitar da historica pop") do
  @proposta.clicar_aceitar(".botaoVerde")
end

Quando("clico no botao aceitar da ficha validacoes pop") do
  @proposta.clicar_aceitar_Verif("#btnAceitar")
  @proposta.clicar_botao_ok_procedencias
end

Quando("preencho os campos de procedencia pop") do
  @proposta.preencher_procedencia
  @proposta.preencher_procedencia_prof
  @proposta.preencher_procedencia_fam
  @proposta.selecionar_correspondencia
end

Quando("clico no botao reprocessar travas da ficha pop") do
  @proposta.clicar_reprocessar_travas
  @proposta.clicar_botao_ok_procedencias
  @proposta.clicar_ok_alerta
end

Quando("clico no botao da aba ficha verificacoes pop") do
  @proposta.clicar_aba_ficha_verificacoes
end

Quando("clico no botao aceitar da validacoes pop") do
  @proposta.clicar_aceitar_Verif("#btnAceitar")
  @proposta.validar_alert_js
end

Quando("clico no botao da aba de decisao pop") do
  @proposta.clicar_aba_decisao
end

Quando("clico no botao aceitar da tela de decisao pop") do
  @proposta.clicar_aceitar(".botaoVerde")
end

Quando("preencho o campo de parecer final pop") do
  @proposta.desabilitar_checked
  @proposta.preencher_parecer_final
end

Quando("clico no botao validar da tela de resumo pop") do
  @proposta.clicar_validar_proposta
  @proposta.validar_alert_js
end

Entao("eu valido que a proposta foi aprovada") do
  find("font", text: "APROVADA")
end
