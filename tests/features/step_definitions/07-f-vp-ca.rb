Entao('seleciono as opcoes de simulador vp com alcada') do
    @proposta.selecionar_tipo_financiamento('CDC')
    @proposta.selecionar_tabela('70')
    @proposta.selecionar_produto('VEÍCULO PESADO')
    @proposta.selecionar_tipo_credito('NORMAL')
    @proposta.selecionar_objeto_financiamento('BCO CDC-VP-B-NR-R4')
end

Quando('preencho os dados do simulador do vp com alcada') do
    @proposta.preencher_dados_simulador_veiculo_pesado

    within_frame(find(:xpath, "//iframe")[:id]) do
        expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
    end
end

Quando('preencho os dados da proposta de negocio do simulador vp com alcada') do
    @proposta.preencher_proposta_negocio_simulador('veiculo pesado')
    @util.pegar_dados_parcela
    @proposta.clicar_preencher_proposta
end

Quando('o cpf do cliente vp com alcada') do
    @proposta.preencher_cpf_cliente(@cpf)
end

Entao('clico no botao validar para validar as informacoes vp com alcada') do
    @proposta.clicar_botao_validar_cliente

    within_frame(find(:xpath, "//iframe")[:id]) do
        textotitulo = find(:xpath, "//tbody/tr/td/font[@class='txtAzulBold12']")
        expect(/Consulta Base Histórica/.match(textotitulo.text)).to be_truthy
    end
end

Quando('clico em proximo na consulta de base historica vp com alcada') do
    @proposta.clicar_proximo_base_historica
end

Quando('preencho os dados de operador vp com alcada') do
    @proposta.preenche_dados_operador
end

Quando('cadastro de pessoa fisica vp com alcada') do
    @proposta.preenche_dados_pessoais
end

Quando('dados profissionais vp com alcada') do
    @proposta.preenche_dados_profissionais
    @proposta.clicar_ok_alerta
end

Quando('dados do vp com alcada') do
    @proposta.preencher_dados_veiculo_pesado
  end

Quando('dados de referencia vp com alcada') do
    @proposta.preenche_dados_referencias
end

Quando('preencho a proposta de negocios vp com alcada') do
    @proposta.preenche_proposta_de_negocios
end

Quando('clico no botao gravar vp com alcada') do
    begin
        @proposta.processar('Gravar')
        rescue
    end
end

Entao('clico no botao processar travas vp com alcada') do
    begin
        @proposta.processar('Processar Travas')
        @proposta.clicar_ok_alerta
        rescue
    end
end

Quando('acesso a pagina de consulta historica vp com alcada') do
    @proposta.nova_tela
end

Quando('clico no botao aceitar da historica vp com alcada') do
    @proposta.clicar_aceitar
end

Quando('clico no botao aceitar da ficha validacoes vp com alcada') do
    @proposta.clicar_aceitar_Verif("#btnAceitar")
    @proposta.clicar_botao_ok_procedencias
end

Quando('preencho os campos de procedencia vp com alcada') do
    @proposta.preencher_procedencia
    @proposta.preencher_procedencia_prof
    @proposta.preencher_procedencia_fam
    @proposta.selecionar_correspondencia
end

Quando('clico no botao reprocessar travas da ficha vp com alcada') do
    @proposta.clicar_reprocessar_travas
    @proposta.clicar_botao_ok_procedencias
    @proposta.clicar_ok_alerta
end

Quando('clico no botao da aba ficha verificacoes vp com alcada') do
    @proposta.clicar_aba_ficha_verificacoes
end

Quando('clico no botao aceitar da validacoes vp com alcada') do
    @proposta.clicar_aceitar_Verif("#btnAceitar")
    @proposta.validar_alert_js
end

Quando('clico no botao da aba de decisao vp com alcada') do
    @proposta.clicar_aba_decisao
end

Quando('clico no botao aceitar da tela de decisao vp com alcada') do
    @proposta.clicar_aceitar
end

Quando('preencho o campo de parecer final vp com alcada') do
    @proposta.desabilitar_checked
    @proposta.preencher_parecer_final
end

Quando('clico no botao validar da tela de resumo vp com alcada') do
    @proposta.clicar_validar_proposta
    @proposta.validar_alert_js
end

Quando('seleciono o usuario com alcada vp com alcada') do
    @proposta.selecionar_usuario
    @proposta.clicar_validar_proposta
end