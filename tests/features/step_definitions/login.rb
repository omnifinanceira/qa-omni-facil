Quando("eu acesso a pagina do Omnifacil") do
  @logininst = Login.new
  @logininst.load
end

Quando("preencho o nome do {string}") do |usuariocucumber|
  @usuario = usuariocucumber
  @logininst.preencher_usuario(usuariocucumber)
end

Quando("preencho a {string}") do |senhacucumber|
  @logininst.preencher_senha(senhacucumber)
end

Quando("clico no botao Conectar") do
  @logininst.clicar_conectar
end

Entao("valido se consegui acessar o Omnifacil") do
  textousuario = find(:xpath, "//a[@href='javascript:;']")
  expect(@usuario.upcase == textousuario.text.strip).to be true
end

Entao("valido o {string} de retorno") do |alerta|
  msgalerta = find(:xpath, "//div[@class='jconfirm-content-pane']", wait: 10)
  if alerta == "bloqueado"
    expect(/Usuário Bloqueado!/.match(msgalerta.text)).to be_truthy
  elsif alerta == "senha"
    expect("Usuário e/ou senha inválido".match(msgalerta.text)).to be_truthy
  elsif alerta == "inexiste"
    expect("Usuário e/ou senha inválido".match(msgalerta.text)).to be_truthy
  elsif alerta == "expirou"
    expect(/Sua Senha Expirou!!/.match(msgalerta.text)).to be_truthy
  elsif alerta == "semusuario"
    expect(/Favor preencher o nome do usuário/.match(msgalerta.text)).to be_truthy
  else alerta == "semsenha"
    expect(/Favor preencher a senha/.match(msgalerta.text)).to be_truthy   end
end
