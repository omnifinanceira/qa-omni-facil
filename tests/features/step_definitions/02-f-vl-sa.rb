Entao("seleciono as opcoes de simulador vl sem alcada") do
  @proposta.selecionar_tipo_financiamento("CDC")
  @proposta.selecionar_tabela("70")
  @proposta.selecionar_produto("VEÍCULO LEVE")
  @proposta.selecionar_tipo_credito("NORMAL")
  @proposta.selecionar_objeto_financiamento("CDC-VL-B BAT-NR-R4")
end

Quando("preencho os dados do simulador do vl sem alcada") do
  @proposta.preencher_dados_simulador_veiculo_leve

  within_frame(find(:xpath, "//iframe")[:id]) do
    expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
  end
end

Quando("preencho os dados da proposta de negocio do simulador vl sem alcada") do
  @proposta.preencher_proposta_negocio_simulador("veiculo leve")
  @util.pegar_dados_parcela
  @proposta.clicar_preencher_proposta
end

Quando("o cpf do cliente vl sem alcada") do
  @proposta.preencher_cpf_cliente(@cpf)
end

Entao("clico no botao validar para validar as informacoes vl sem alcada") do
  @proposta.clicar_botao_validar_cliente

  within_frame(find(:xpath, "//iframe")[:id]) do
    textotitulo = find(:xpath, "//tbody/tr/td/font[@class='txtAzulBold12']")
    expect(/Consulta Base Histórica/.match(textotitulo.text)).to be_truthy
  end
end

Quando("clico em proximo na consulta de base historica vl sem alcada") do
  @proposta.clicar_proximo_base_historica
end

Quando("preencho os dados de operador vl sem alcada") do
  @proposta.preenche_dados_operador
end

Quando("cadastro de pessoa fisica vl sem alcada") do
  @proposta.preenche_dados_pessoais
end

Quando("dados profissionais vl sem alcada") do
  @proposta.preenche_dados_profissionais
  @proposta.clicar_ok_alerta
end

Quando("dados do vl sem alcada") do
  @proposta.preencher_dados_veiculo_leve
end

Quando("dados de referencia vl sem alcada") do
  @proposta.preenche_dados_referencias
end

Quando("preencho a proposta de negocios vl sem alcada") do
  @proposta.preenche_proposta_de_negocios
end

Quando("clico no botao gravar vl sem alcada") do
  begin
    @proposta.processar("Gravar")
  rescue
  end
end

Entao("clico no botao processar travas vl sem alcada") do
  begin
    @proposta.processar("Processar Travas")
    #binding.pry
    @proposta.clicar_ok_alerta
  rescue
  end
end

Quando("acesso a pagina de consulta historica vl sem alcada") do
  @proposta.nova_tela
end

Quando("clico no botao enviar para mesa da vl sem alcada") do
  binding.pry
  @proposta.clicar_enviar_para_mesa
end

Entao("eu valido que a proposta foi gerada com sucesso") do
  find("font", text: "Proposta gerada com sucesso!")
end

# Quando('preencho os campos de procedencia vl') do
#     @proposta.preencher_procedencia
#     @proposta.preencher_procedencia_prof
#     @proposta.preencher_procedencia_fam
#     @proposta.selecionar_correspondencia
# end

# Quando('clico no botao reprocessar travas da ficha vl') do
#     @proposta.clicar_reprocessar_travas
#     @proposta.clicar_botao_ok_procedencias
#     @proposta.clicar_ok_alerta
# end

# Quando('clico no botao da aba ficha verificacoes vl') do
#     @proposta.clicar_aba_ficha_verificacoes
# end

# Quando('clico no botao aceitar da validacoes vl') do
#     @proposta.clicar_aceitar_Verif("#btnAceitar")
#     @proposta.validar_alert_js
# end

# Quando('clico no botao da aba de decisao vl') do
#     @proposta.clicar_aba_decisao
# end

# Quando('clico no botao aceitar da tela de decisao vl') do
#     @proposta.clicar_aceitar
# end

# Quando('preencho o campo de parecer final vl') do
#     @proposta.desabilitar_checked
#     @proposta.preencher_parecer_final
# end

# Quando('clico no botao validar da tela de resumo vl') do
#     @proposta.clicar_validar_proposta
#     @proposta.validar_alert_js
# end

# Quando('seleciono o usuario com alcada vl') do
#     @proposta.selecionar_usuario
#     @proposta.clicar_validar_proposta
# end
