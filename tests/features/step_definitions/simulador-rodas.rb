# Quando('eu acesso o novo menu') do
#   sql = Queries.new
#   @cpf = sql.pegar_cpf

#   home = Home.new
#   home.acessar_novo_menu
#   @util = Util.new
# end

# Quando('seleciono o {string}') do |agente|
#   @proposta = SimuladorRodas.new
#   @proposta.selecionar_agente(agente)
#   @proposta.clicar_validar
# end

# Entao('seleciono o {string}, {string} e {string}') do |menu, submenu, subsubmenu|
#   @proposta.selecionar_menu(menu)
#   @proposta.selecionar_submenu(submenu)
#   @proposta.selecionar_submenu(subsubmenu)
# end

# Entao('seleciono as opcoes de simulador veiculo leve') do
#   @proposta.selecionar_tipo_financiamento('CDC')
#   @proposta.selecionar_tabela('70')
#   @proposta.selecionar_produto('VEÍCULO LEVE')
#   @proposta.selecionar_tipo_credito('NORMAL')
#   @proposta.selecionar_objeto_financiamento('CDC-VL-B-NR-R4')
# end

# Entao('seleciono as opcoes de simulador moto') do
#   @proposta.selecionar_tipo_financiamento('CDC')
#   @proposta.selecionar_tabela('70')
#   @proposta.selecionar_produto('MOTO')
#   @proposta.selecionar_tipo_credito('NORMAL')
#   @proposta.selecionar_objeto_financiamento('CDC-MT-B-NR-R4')
# end

# Entao('seleciono as opcoes de simulador veiculo pesado') do
#   @proposta.selecionar_tipo_financiamento('CDC')
#   @proposta.selecionar_tabela('70')
#   @proposta.selecionar_produto('VEÍCULO PESADO')
#   @proposta.selecionar_tipo_credito('NORMAL')
#   @proposta.selecionar_objeto_financiamento('BCO CDC-VP-B-NR-R4')
# end

# # BUSCAR OS DADOS DE POP PARA INSERIR AQUI
# Entao('seleciono as opcoes de simulador pop') do
#   @proposta.selecionar_tipo_financiamento('CDC')
#   @proposta.selecionar_tabela('70')
#   @proposta.selecionar_produto('POP')
#   @proposta.selecionar_tipo_credito('NORMAL')
#   @proposta.selecionar_objeto_financiamento('BCO CDC-POP-B-NR-R4')
# end

# Quando('preencho os dados do simulador do veiculo leve') do
#   @proposta.preencher_dados_simulador_veiculo_leve
  
#   within_frame(find(:xpath, "//iframe")[:id]) do
#     expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
#   end
# end

# Quando('preencho os dados do simulador da moto') do
#   @proposta.preencher_dados_simulador_moto
  
#   within_frame(find(:xpath, "//iframe")[:id]) do
#     expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
#   end
# end

# Quando('preencho os dados do simulador do veiculo pesado') do
#   @proposta.preencher_dados_simulador_veiculo_pesado
  
#   within_frame(find(:xpath, "//iframe")[:id]) do
#     expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
#   end
# end

# # VERIFICAR SE ESTE CAMPO É O MESMO PARA POP
# Quando('preencho os dados do simulador do pop') do
#   @proposta.preencher_dados_simulador_pop
  
#   within_frame(find(:xpath, "//iframe")[:id]) do
#     expect(find(:xpath, "//input[@name='P_PROP_VLR_COMP']")[:value]).not_to eq(nil)
#   end
# end

# Quando('preencho os dados da proposta de negocio do simulador veiculo leve') do
#   @proposta.preencher_proposta_negocio_simulador('veiculo leve')
#   @util.pegar_dados_parcela
#   @proposta.clicar_preencher_proposta
# end

# Quando('preencho os dados da proposta de negocio do simulador moto') do
#   @proposta.preencher_proposta_negocio_simulador('moto')
#   @util.pegar_dados_parcela
#   @proposta.clicar_preencher_proposta
# end

# Quando('preencho os dados da proposta de negocio do simulador veiculo pesado') do
#   @proposta.preencher_proposta_negocio_simulador('veiculo pesado')
#   @util.pegar_dados_parcela
#   @proposta.clicar_preencher_proposta
# end

# Quando('preencho os dados da proposta de negocio do simulador pop') do
#   @proposta.preencher_proposta_negocio_simulador('pop')
#   @util.pegar_dados_parcela
#   @proposta.clicar_preencher_proposta
# end

# Quando('o cpf do cliente') do
#   @proposta.preencher_cpf_cliente(@cpf)
# end

# Entao('clico no botao validar para validar as informacoes') do
#   @proposta.clicar_botao_validar_cliente
  
#   within_frame(find(:xpath, "//iframe")[:id]) do
#     textotitulo = find(:xpath, "//tbody/tr/td/font[@class='txtAzulBold12']")
#       expect(/Consulta Base Histórica/.match(textotitulo.text)).to be_truthy
#   end
# end

# Quando('clico em proximo na consulta de base historica') do
#   @proposta.clicar_proximo_base_historica
# end

# Quando('preencho os dados de operador') do
#   @proposta.preenche_dados_operador
# end

# Quando('cadastro de pessoa fisica') do
#   @proposta.preenche_dados_pessoais
# end

# Quando('dados profissionais') do
#   @proposta.preenche_dados_profissionais
#   @proposta.clicar_ok_alerta
# end

# Quando('dados do veiculo leve') do
#   @proposta.preencher_dados_veiculo_leve
# end

# Quando('dados da moto') do
#   @proposta.preencher_dados_moto
# end

# Quando('dados do veiculo pesado') do
#   @proposta.preencher_dados_veiculo_pesado
# end

# Quando('dados do pop') do
#   @proposta.preencher_dados_pop
# end

# Quando('dados de referencia') do
#   @proposta.preenche_dados_referencias
# end

# Quando('preencho a proposta de negocios') do
#   @proposta.preenche_proposta_de_negocios
# end

# Quando('clico no botao gravar') do
#   begin
#     @proposta.processar('Gravar')
#   rescue
#   end
# end

# Entao('clico no botao processar travas') do
#   begin
#     @proposta.processar('Processar Travas')
#     @proposta.clicar_ok_alerta
#   rescue
#   end
# end

# Quando('acesso a pagina de consulta historica') do
#   @proposta.nova_tela
# end

# Quando('clico no botao aceitar da historica') do
#   @proposta.clicar_aceitar
# end

# Quando('clico no botao aceitar da ficha validacoes') do
#   @proposta.clicar_aceitar_Verif("#btnAceitar")
#   @proposta.clicar_botao_ok_procedencias
# end

# Quando('preencho os campos de procedencia') do
#   @proposta.preencher_procedencia
#   @proposta.preencher_procedencia_prof
#   @proposta.preencher_procedencia_fam
#   @proposta.selecionar_correspondencia
# end

# Quando('clico no botao reprocessar travas da fichas') do
#   @proposta.clicar_reprocessar_travas
#   @proposta.clicar_botao_ok_procedencias
#   @proposta.clicar_ok_alerta
# end

# Quando('clico no botao da aba ficha verificacoes') do
#   @proposta.clicar_aba_ficha_verificacoes
# end

# Quando('clico no botao aceitar da validacoes') do
#   @proposta.clicar_aceitar_Verif("#btnAceitar")
#   @proposta.validar_alert_js
# end

# Quando('clico no botao da aba de decisao') do
#   @proposta.clicar_aba_decisao
# end

# Quando('clico no botao aceitar da tela de decisao') do
#   @proposta.clicar_aceitar
# end

# Quando('preencho o campo de parecer final') do
#   @proposta.desabilitar_checked
#   @proposta.preencher_parecer_final
# end

# Quando('clico no botao validar da tela de resumo') do
#   @proposta.clicar_validar_proposta
#   @proposta.validar_alert_js
# end

# Quando('seleciono o usuario com alcada') do
#   @proposta.selecionar_usuario
#   @proposta.clicar_validar_proposta
# end

