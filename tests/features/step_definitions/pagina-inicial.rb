Dado("que eu esteja logado") do
  @login = Login.new
  @login.load
  @login.acessar_omni_facil
end

Quando("eu espero visualizar a home do omni facil") do
  # Localiza elemento na pagina atual
  expect(page.has_xpath?("//i[@class='fa fa-calendar']")).to eq true
end

Entao("valido os campos existentes") do
  titulousuario = find(:xpath, "//div[@class='col-xs-10 col-sm-8']")
  usuarioregex = $usuario.gsub(/[0-9]/, "")
  expect(titulousuario.text).to include(usuarioregex)
  # Valida a Logo da Omni no topo da página
  expect(page.has_xpath?("//a[@id='logo-omni']")).to eq true
  # Valida o calendário no topo da págin
  expect(page.has_xpath?("//i[@class='fa fa-calendar']")).to eq true
  # Valida o Nome do Usuário que está Logado
  textousuario = find(:xpath, "//a[@href='javascript:;']")
  expect(textousuario.text).to include(usuarioregex)
end
