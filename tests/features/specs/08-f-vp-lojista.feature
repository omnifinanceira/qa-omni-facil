#language:pt
@08 @f-vp-lojista
Funcionalidade: Gerar proposta de financiamento veiculo pesado com logista

Contexto:
    Dado que eu esteja logado "LOJISTA1"

Cenario: Gerar proposta de financiamento veiculo pesado com logista
    Quando eu acesso a tela inicial
    E clico na aba de simulador
    E preencho o campo cpf do cliente
    E seleciono o produto
    E preencho os dados do veiculo no simulador
    E preencho os dados da proposta de negocio no simulador
    E clico no botao preencher proposta