#language:pt
@01 @f-pop
Funcionalidade: Gerar proposta de financiamento pop

Contexto:
    Dado que eu esteja logado

Cenario: Gerar proposta de financiamento pop
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador pop
    E preencho os dados do simulador do pop
    E preencho os dados da proposta de negocio do simulador pop
    E o cpf do cliente pop
    E clico no botao validar para validar as informacoes pop
    E clico em proximo na consulta de base historica pop
    E preencho os dados de operador pop
    E cadastro de pessoa fisica pop
    E dados profissionais pop
    E dados do pop
    E dados de referencia pop
    E preencho a proposta de negocios pop
    E clico no botao gravar pop
    E clico no botao processar travas pop
    E acesso a pagina de consulta historica pop
    E clico no botao aceitar da historica pop
    E clico no botao aceitar da ficha validacoes pop
    E preencho os campos de procedencia pop
    E clico no botao reprocessar travas da ficha pop
    E clico no botao da aba ficha verificacoes pop
    E clico no botao aceitar da validacoes pop
    E clico no botao da aba de decisao pop
    E clico no botao aceitar da tela de decisao pop
    E preencho o campo de parecer final pop
    E clico no botao validar da tela de resumo pop
    Entao eu valido que a proposta foi aprovada

Exemplos:
#|agente                                       |menu         |submenu  |subsubmenu |
#|"1247 - MOGI DAS CRUZES - MOGI DAS CRUZES-SP"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|
 |agente                         |menu         |submenu  |subsubmenu |
 |"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|
