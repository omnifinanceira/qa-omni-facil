#language:pt
@05 @f-mt-ca
Funcionalidade: Gerar proposta de financiamento moto

Contexto:
    Dado que eu esteja logado

Cenario: Gerar proposta de financiamento moto
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador mt com alcada
    E preencho os dados do simulador da mt com alcada
    E preencho os dados da proposta de negocio do simulador mt com alcada
    E o cpf do cliente mt com alcada
    E clico no botao validar para validar as informacoes mt com alcada
    E clico em proximo na consulta de base historica mt com alcada
    E preencho os dados de operador mt com alcada
    E cadastro de pessoa fisica mt com alcada
    E dados profissionais mt com alcada
    E dados da mt com alcada
    E dados de referencia mt com alcada
    E preencho a proposta de negocios mt com alcada
    E clico no botao gravar mt com alcada
    Entao clico no botao processar travas mt com alcada
    E acesso a pagina de consulta historica mt com alcada
    E clico no botao aceitar da historica mt com alcada
    E clico no botao aceitar da ficha validacoes mt com alcada
    E preencho os campos de procedencia mt com alcada
    E clico no botao reprocessar travas da ficha mt com alcada
    E clico no botao da aba ficha verificacoes mt com alcada
    E clico no botao aceitar da validacoes mt com alcada
    E clico no botao da aba de decisao mt com alcada
    E clico no botao aceitar da tela de decisao mt com alcada
    E preencho o campo de parecer final mt com alcada
    E clico no botao validar da tela de resumo mt com alcada
    E seleciono o usuario com alcada mt com alcada

Exemplos:
|agente                         |menu         |submenu  |subsubmenu |
|"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|
#ALTERADO 07/05/2021 - VICTOR RIBEIRO
#|"1247 - MOGI DAS CRUZES - MOGI DAS CRUZES-SP"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|