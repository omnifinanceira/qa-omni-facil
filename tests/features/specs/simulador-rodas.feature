#language:pt
@2 @novo-menu
Funcionalidade: Criar proposta

Contexto:
    Dado que eu esteja logado

@pop
Cenario: Gerar proposta pop
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador pop
    E preencho os dados do simulador do pop
    E preencho os dados da proposta de negocio do simulador pop
    E o cpf do  pop
    E clico no botao validar para validar as informacoes pop
    E clico em proximo na consulta de base historica pop
    E preencho os dados de operador pop
    E cadastro de pessoa fisica pop
    E dados profissionais pop
    E dados do pop
    E dados de referencia pop
    E preencho a proposta de negocios pop
    E clico no botao gravar pop
    Entao clico no botao processar travas pop
    E acesso a pagina de consulta historica pop
    E clico no botao aceitar da historica pop
    E clico no botao aceitar da ficha validacoes pop
    E preencho os campos de procedencia pop
    E clico no botao reprocessar travas da fichas pop
    E clico no botao da aba ficha verificacoes pop
    E clico no botao aceitar da validacoes pop
    E clico no botao da aba de decisao pop
    E clico no botao aceitar da tela de decisao pop
    E preencho o campo de parecer final pop
    E clico no botao validar da tela de resumo pop
    E seleciono o usuario com alcada pop

Exemplos:
|agente                         |menu         |submenu  |subsubmenu |
|"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|


# @moto
# Cenario: Gerar proposta moto
#     Quando eu acesso o novo menu
#     E seleciono o <agente>
#     E seleciono o <menu>, <submenu> e <subsubmenu>
#     E seleciono as opcoes de simulador moto
#     E preencho os dados do simulador da moto
#     E preencho os dados da proposta de negocio do simulador moto
#     E o cpf do cliente
#     E clico no botao validar para validar as informacoes
#     E clico em proximo na consulta de base historica
#     E preencho os dados de operador
#     E cadastro de pessoa fisica
#     E dados profissionais
#     E dados da moto
#     E dados de referencia
#     E preencho a proposta de negocios
#     Entao clico no botao gravar e valido a mensagem de alerta
#     #Entao clico no botao processar travas e valido a mensagem de alerta

# Exemplos:
# |agente                         |menu         |submenu  |subsubmenu |
# |"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|

# @veiculo-pesado
# Cenario: Gerar proposta veiculo pesado
#     Quando eu acesso o novo menu
#     E seleciono o <agente>
#     E seleciono o <menu>, <submenu> e <subsubmenu>
#     E seleciono as opcoes de simulador veiculo pesado
#     E preencho os dados do simulador do veiculo pesado
#     E preencho os dados da proposta de negocio do simulador veiculo pesado
#     E o cpf do cliente
#     E clico no botao validar para validar as informacoes
#     E clico em proximo na consulta de base historica
#     E preencho os dados de operador
#     E cadastro de pessoa fisica
#     #E confirmo procedencia dados pessoais
#     E dados profissionais
#     #E confirmo procedencia dados profissionais
#     E dados do veiculo pesado
#     E dados de referencia
#     #E confirmo procedencia referencia
#     E preencho a proposta de negocios
#     Entao clico no botao gravar e valido a mensagem de alerta
#     #Entao clico no botao processar travas e valido a mensagem de alerta

# Exemplos:
# |agente                         |menu         |submenu  |subsubmenu |
# |"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|