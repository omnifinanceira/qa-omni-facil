#language:pt
@02 @f-vl-sa
Funcionalidade: Gerar proposta de financiamento veiculo leve

Contexto:
    Dado que eu esteja logado

Cenario: Gerar proposta de financiamento veiculo leve
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador vl sem alcada
    E preencho os dados do simulador do vl sem alcada
    E preencho os dados da proposta de negocio do simulador vl sem alcada
    E o cpf do cliente vl sem alcada
    E clico no botao validar para validar as informacoes vl sem alcada
    E clico em proximo na consulta de base historica vl sem alcada
    E preencho os dados de operador vl sem alcada
    E cadastro de pessoa fisica vl sem alcada
    E dados profissionais vl sem alcada
    E dados do vl sem alcada
    E dados de referencia vl sem alcada
    E preencho a proposta de negocios vl sem alcada
    E clico no botao gravar vl sem alcada
    E clico no botao processar travas vl sem alcada
    E clico no botao enviar para mesa da vl sem alcada
    Entao eu valido que a proposta foi gerada com sucesso


Exemplos:
|agente                                       |menu         |submenu  |subsubmenu |
#|"1247 - MOGI DAS CRUZES - MOGI DAS CRUZES-SP"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|
# |agente                         |menu         |submenu  |subsubmenu |
|"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|