#language:pt
@pagina-inicial
Funcionalidade: Validar campos da pagina inicial

Contexto:
    Dado que eu esteja logado

Cenario: Verificar os campos existentes da pagina inicial
    Quando eu espero visualizar a home do omni facil
    Entao valido os campos existentes
