#language: pt
@login
Funcionalidade: Login

@login-sucesso
Cenario: Testar um login com sucesso
    Quando eu acesso a pagina do Omnifacil
    E preencho o nome do <usuario>
    E preencho a <senha>
    E clico no botao Conectar
    Entao valido se consegui acessar o Omnifacil

Exemplos:
    | usuario         | senha          |
    | "184BATISTELLA" | "Y3HYVMHT" |
    #| "eliana"        | "S@LVADORA1"   |
    #| "00184LZENITHE" | "2WQAAVBZNKUF" | Tela do Lojista está sem o Bootstrap (verificar)

@login-insucesso
Cenario: Testar um login de insucesso
    Quando eu acesso a pagina do Omnifacil
    E preencho o nome do <usuario>
    E preencho a <senha>
    E clico no botao Conectar
    Entao valido o <alerta> de retorno

Exemplos:
    | usuario      | senha          | alerta       |
    | "1247WAGNER" | "1234"         | "senha"      |
    | "naoexiste"   | "HOMOLOG2020"  | "inexiste"   |
    | "184IVONE"   | "B7GMQA9MA3EG" | "expirou"    |
    | ""           | "senha123"     | "semusuario" |
    | "1247WAGNER" | ""             | "semsenha"   |  
    #|              |                |              |#Colocar um usuario bloqueado
