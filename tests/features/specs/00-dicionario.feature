#language:pt

Funcionalidade: Traduzir abreviação

Cenario: Traduzir abreviação

Exemplos:
|Sigla     |Traducao              |
|"01-f-pop"|"01-financiamento-pop"|
|"02-f-vl-sa"|"02-financiamento-veiculo-leve-sem-alcada"|
|"03-f-vl-ca"|"02-financiamento-veiculo-leve-com-alcada"|
|"04-f-mt-sa"|"02-financiamento-moto-sem-alcada"|
|"05-f-mt-ca"|"02-financiamento-moto-com-alcada"|
|"06-f-vp-sa"|"02-financiamento-veiculo-pesado-sem-alcada"|
|"07-f-vp-ca"|"02-financiamento-veiculo-pesado-com-alcada"|