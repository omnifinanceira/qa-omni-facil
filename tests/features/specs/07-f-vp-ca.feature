#language:pt
@07 @f-vp-ca
Funcionalidade: Gerar proposta de financiamento veiculo pesado com alcada

Contexto:
    Dado que eu esteja logado "SUPERVISOR"

Cenario: Gerar proposta de financiamento veiculo pesado com alcada
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador vp com alcada
    E preencho os dados do simulador do vp com alcada
    E preencho os dados da proposta de negocio do simulador vp com alcada
    E o cpf do cliente vp com alcada
    E clico no botao validar para validar as informacoes vp com alcada
    E clico em proximo na consulta de base historica vp com alcada
    E preencho os dados de operador vp com alcada
    E cadastro de pessoa fisica vp com alcada
    E dados profissionais vp com alcada
    E dados do vp com alcada
    E dados de referencia vp com alcada
    E preencho a proposta de negocios vp com alcada
    E preencho os campos de procedencia vp com alcada
    E clico no botao gravar vp com alcada
    Entao clico no botao processar travas vp com alcada
    E acesso a pagina de consulta historica vp com alcada
    E clico no botao aceitar da historica vp com alcada
    E clico no botao aceitar da ficha validacoes vp com alcada
    #E preencho os campos de procedencia vp com alcada
    E clico no botao reprocessar travas da ficha vp com alcada
    E clico no botao da aba ficha verificacoes vp com alcada
    E clico no botao aceitar da validacoes vp com alcada
    E clico no botao da aba de decisao vp com alcada
    E clico no botao aceitar da tela de decisao vp com alcada
    E preencho o campo de parecer final vp com alcada
    E clico no botao validar da tela de resumo vp com alcada
    E seleciono o usuario com alcada vp com alcada

Exemplos:
|agente                         |menu         |submenu  |subsubmenu |
|"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|