#language:pt
@04 @f-mt-sa
Funcionalidade: Gerar proposta de financiamento moto

Contexto:
    Dado que eu esteja logado "AGENTE"

Cenario: Gerar proposta de financiamento moto
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador mt sem alcada
    E preencho os dados do simulador da mt sem alcada
    E preencho os dados da proposta de negocio do simulador mt sem alcada
    E o cpf do cliente mt sem alcada
    E clico no botao validar para validar as informacoes mt sem alcada
    E clico em proximo na consulta de base historica mt sem alcada
    E preencho os dados de operador mt sem alcada
    E cadastro de pessoa fisica mt sem alcada
    E dados profissionais mt sem alcada
    E dados da mt sem alcada
    E dados de referencia mt sem alcada
    E preencho a proposta de negocios mt sem alcada
    E clico no botao gravar mt sem alcada
    Entao clico no botao processar travas mt sem alcada
    E acesso a pagina de consulta historica mt sem alcada
    E clico no botao enviar para mesa da mt sem alcada

    # E clico no botao aceitar da ficha validacoes mt
    # E preencho os campos de procedencia mt
    # E clico no botao reprocessar travas da ficha mt
    # E clico no botao da aba ficha verificacoes mt
    # E clico no botao aceitar da validacoes mt
    # E clico no botao da aba de decisao mt
    # E clico no botao aceitar da tela de decisao mt
    # E preencho o campo de parecer final mt
    # E clico no botao validar da tela de resumo mt
    # E seleciono o usuario com alcada mt

Exemplos:
|agente                         |menu         |submenu  |subsubmenu |
|"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|