#language:pt
@03 @f-vl-ca
Funcionalidade: Gerar proposta de financiamento veiculo leve

Contexto:
    Dado que eu esteja logado

Cenario: Gerar proposta de financiamento veiculo leve
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador vl com alcada
    E preencho os dados do simulador do vl com alcada
    E preencho os dados da proposta de negocio do simulador vl com alcada
    E o cpf do cliente vl com alcada
    E clico no botao validar para validar as informacoes vl com alcada
    E clico em proximo na consulta de base historica vl com alcada
    E preencho os dados de operador vl com alcada
    E cadastro de pessoa fisica vl com alcada
    E dados profissionais vl com alcada
    E dados do vl com alcada
    E dados de referencia vl com alcada
    E preencho a proposta de negocios vl com alcada
    E clico no botao gravar vl com alcada
    Entao clico no botao processar travas vl com alcada
    E acesso a pagina de consulta historica vl com alcada
    E clico no botao aceitar da historica vl com alcada
    E clico no botao aceitar da ficha validacoes vl com alcada
    E preencho os campos de procedencia vl com alcada
    E clico no botao reprocessar travas da ficha vl com alcada
    E clico no botao da aba ficha verificacoes vl com alcada
    E clico no botao aceitar da validacoes vl com alcada
    E clico no botao da aba de decisao vl com alcada
    E clico no botao aceitar da tela de decisao vl com alcada
    E preencho o campo de parecer final vl com alcada
    E clico no botao validar da tela de resumo vl com alcada
    E seleciono o usuario com alcada vl com alcada

Exemplos:
|agente                                       |menu         |submenu  |subsubmenu |
#|"1247 - MOGI DAS CRUZES - MOGI DAS CRUZES-SP"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|
#|agente                         |menu         |submenu  |subsubmenu |
|"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|