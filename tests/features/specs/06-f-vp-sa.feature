#language:pt
@06 @f-vp-sa
Funcionalidade: Gerar proposta de financiamento veiculo pesado sem alcada

Contexto:
    Dado que eu esteja logado

Cenario: Gerar proposta de financiamento veiculo pesado sem alcada
    Quando eu acesso o novo menu
    E seleciono o <agente>
    E seleciono o <menu>, <submenu> e <subsubmenu>
    E seleciono as opcoes de simulador vp sem alcada
    E preencho os dados do simulador do vp sem alcada
    E preencho os dados da proposta de negocio do simulador vp sem alcada
    E o cpf do cliente vp sem alcada
    E clico no botao validar para validar as informacoes vp sem alcada
    E clico em proximo na consulta de base historica vp sem alcada
    E preencho os dados de operador vp sem alcada
    E cadastro de pessoa fisica vp sem alcada
    E dados profissionais vp sem alcada
    E dados do vp sem alcada
    E dados de referencia vp sem alcada
    E preencho a proposta de negocios vp sem alcada
    E clico no botao gravar vp sem alcada
    E clico no botao processar travas vp sem alcada
    E clico no botao enviar para mesa da vp sem alcada
    Entao eu valido que a proposta foi gerada com sucesso

Exemplos:
|agente                                       |menu         |submenu  |subsubmenu |
#|"1247 - MOGI DAS CRUZES - MOGI DAS CRUZES-SP"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|
 |"184 - BATISTELLA - MARINGA-PR"|"OPERACIONAL"|"CRÉDITO"|"Simulador"|