require_relative "util.rb"

class Login < Util
  set_url "/pck_login.prc_login"

  element :elementinputusuario, :xpath, "//input[@id='p-nome']"
  element :elementinputsenha, :xpath, "//input[@id='p-senha']"
  element :elementbuttonconectar, :xpath, "//button[@id='btn-conectar']"

  def preencher_usuario(usuario = $usuario)
    elementinputusuario.set(usuario)
  end

  def preencher_senha(senha = $senha)
    elementinputsenha.set(senha)
  end

  def clicar_conectar
    elementbuttonconectar.click
  end

  def acessar_omni_facil
    preencher_usuario($usuario)
    preencher_senha($senha)
    clicar_conectar
  end
end
