require 'pdf-reader'

class Pdf

  def initialize
    @textvet = []
  end

  def ler_pdf
      reader = PDF::Reader.new(Dir.glob('C:/users/*.*/Downloads/File*.pdf').first)

      reader.pages.each do |page|
        @textvet << page.text.split(' ')
      end
  end

  def quantidade_parcela
    quantidade = @textvet.first[@textvet.first.index("parcelas:")+1]

    return quantidade
  end

  def valor_parcela
    valor = @textvet.first[@textvet.first.index("parcela:")+2]

    return valor
  end

end