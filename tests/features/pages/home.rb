class Home < SitePrism::Page

    set_url ''

    element :elementbuttonnovomenu, :xpath, "//a[@url='pck_bs.prc_menu_bs']"
    element :elementbuttonadiantamentos, :xpath, "//a[@url='PCK_ADIANTAMENTO.PRINCIPAL']"
    element :elementbuttonomnijur, :xpath, "//a[@url='PCK_OMNIJUR.PRINCIPAL']"
    element :elementbuttonhistorica, :xpath, "//a[@url='pck_consulta_historica.parametros_consulta']"
    element :elementbuttonlojista, :xpath, "//a[@url='pck_lojista.principal']"
    element :elementbuttonmesa, :xpath, "//a[@url='pck_mesa_credito.principal']"
    element :elementbuttonomniempresas, :xpath, "//a[@class='btn btn-app']/span[text()='OMNI EMPRESAS ']"
    element :elementbuttonchamado, :xpath, "//a[@url='WEB_CHAMADO.FRAME_PRINCIPAL']"
    element :elementbuttonranking, :xpath, "//a[@url='PCK_RANKING_PROMOTOR_N.PRINCIPAL']"
    element :elementbuttonsubstituicao, :xpath, "//a[@url='pck_substituicao_garantia.principal']"

    def acessar_novo_menu
        elementbuttonnovomenu.click
    end
    
    def acessar_adiantamentos
        elementbuttonadiantamentos.click
    end

    def acessar_omni_jur
        elementbuttonomnijur.click
    end

    def acessar_historica
        elementbuttonhistorica.click
    end

    def acessar_lojista
        elementbuttonlojista.click
    end

    def acessar_mesa
        elementbuttonmesa.click
    end

    def acessar_omni_empresas
        elementbuttonomniempresas.click
    end

    def acessar_chamado
        elementbuttonchamado.click
    end

    def acessar_ranking
        elementbuttonranking.click
    end

    def acessar_substituicao
        elementbuttonsubstituicao.click
    end
    
end