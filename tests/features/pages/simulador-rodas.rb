require_relative "geradorrandomico.rb"

class SimuladorRodas < GeradorRandomico
  set_url "/pck_login.prc_login"

  element :elementseletoragente, :xpath, "//span[@id='select2-p-agente-container']"
  element :elementbuttonvalidar, :xpath, "//button[@id='bt-validar']"
  element :elementinputvalorliberado, :xpath, "//input[@name='P_PROP_VLR_LIBERADO']"
  element :elementbuttonpreencherproposta, :xpath, "//input[@value='Preencher Proposta']"
  element :elementinputpreenchercpfcliente, :xpath, "//input[@name='P_CPF_CLI']"
  element :elementbuttonvalidarcpfclient, :xpath, "//input[@name='P_BOTAO']"
  element :elementbuttonproximo, :xpath, "//input[@value='Próximo']"
  element :elementbuttonaceitar, :xpath, "//input[@class='botaoVerde']"
  element :elementbuttonok, :xpath, "//div/input[@id='popup_ok']"

  def selecionar_agente(agente)
    elementseletoragente.click
    find(:xpath, "//ul/li[text()='#{agente}']").click
    elementbuttonvalidar.click
  end

  def selecionar_menu(menu)
    find(:xpath, "//li/a[text()=' #{menu}']").click
  end

  def selecionar_submenu(submenu)
    find(:xpath, "//ul/li/a[text()='#{submenu}']").click
  end

  def selecionar_tipo_financiamento(opcao)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@id='p_grupo1']").find(:xpath, "option[@value='#{opcao}']").select_option
    end
  end

  def selecionar_tabela(opcao)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@id='p_tipo_tabela']").find(:xpath, "option[@value='#{opcao}']").select_option
    end
  end

  def selecionar_produto(opcao)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@id='p_grupo2']").find(:xpath, "option[@value='#{opcao}']").select_option
    end
  end

  def selecionar_tipo_credito(opcao)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@id='p_grupo4']").find(:xpath, "option[@value='#{opcao}']").select_option
    end
  end

  def selecionar_objeto_financiamento(opcao)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@id='p_grupo3']").find(:xpath, "option[@value='#{opcao}']").select_option
    end
  end

  # VEÍCULO LEVE
  def preencher_dados_simulador_veiculo_leve
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@name='P_CATEGORIA1']").find(:xpath, "option[@value='1']").select_option
      find(:xpath, "//select[@name='P_MARCA1']").find(:xpath, "option[@value='8']").select_option
      find(:xpath, "//select[@name='P_ANO_MODELO1']").find(:xpath, "option[@value='2015']").select_option
      find(:xpath, "//select[@name='P_MODELO1']").find(:xpath, "option[@value='4390']").select_option
      find(:xpath, "//select[@name='P_VERSAO1']").find(:xpath, "option[@value='26496']").select_option
    end
  end

  # MOTO
  def preencher_dados_simulador_moto
    within_frame(find(:xpath, "//iframe")[:id]) do
      #inding.pry
      find(:xpath, "//select[@name='P_CATEGORIA1']").find(:xpath, "option[@value='5']").select_option
      find(:xpath, "//select[@name='P_MARCA1']").find(:xpath, "option[@value='8']").select_option
      find(:xpath, "//select[@name='P_ANO_MODELO1']").find(:xpath, "option[@value='2017']").select_option
      find(:xpath, "//select[@name='P_MODELO1']").find(:xpath, "option[@value='5164']").select_option
      find(:xpath, "//select[@name='P_VERSAO1']").find(:xpath, "option[@value='30199']").select_option
    end
  end

  # VEÍCULO PESADO
  def preencher_dados_simulador_veiculo_pesado
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@name='P_CATEGORIA1']").find(:xpath, "option[@value='4']").select_option
      find(:xpath, "//select[@name='P_MARCA1']").find(:xpath, "option[@value='2']").select_option
      #########################################################################  RIBEIRO 11/03/2021 - value=2018
      find(:xpath, "//select[@name='P_ANO_MODELO1']").find(:xpath, "option[@value='2010']").select_option
      find(:xpath, "//select[@name='P_MODELO1']").find(:xpath, "option[@value='3688']").select_option
      #########################################################################  RIBEIRO 11/03/2021 - value=23090
      find(:xpath, "//select[@name='P_VERSAO1']").find(:xpath, "option[@value='23073']").select_option
    end
  end

  # POP  >> BUSCAR INFORMAÇÕES NA PROPOSTA PARA INCLUIR AQUI
  def preencher_dados_simulador_pop
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//select[@name='P_CATEGORIA1']").find(:xpath, "option[@value='2']").select_option
      find(:xpath, "//select[@name='P_MARCA1']").find(:xpath, "option[@value='3']").select_option
      find(:xpath, "//select[@name='P_ANO_MODELO1']").find(:xpath, "option[@value='2016']").select_option
      find(:xpath, "//select[@name='P_MODELO1']").find(:xpath, "option[@value='4365']").select_option
      find(:xpath, "//select[@name='P_VERSAO1']").find(:xpath, "option[@value='26321']").select_option
    end
  end

  #def clicar_ok_alerta_tabela_retorno
  # Código utilizado para fechar um Alerta de erro que pode ser exibido antes de selecionar
  # a tabela de retorno na tela do Simulador
  def clicar_ok_alerta_procedencias
    # IMPLEMENTADO EM JANEIRO 2021
    # Código utilizado para fechar um Alerta de erro que é exibido ao ser clicado no botão
    # Aceitar da tela de Ficha/Verificações após Aceitar da Histórica.
    # Os campos de procedência devem ser preenchidos
    begin
      within_frame(find(:xpath, "//iframe")[:id]) do
        while find("#id_popup_message").visible?
          within("#id_popup_message", wait: 5) do
            click_button("OK")
          end
        end
      end
    rescue
    end
  end

  def preencher_proposta_negocio_simulador(veiculo)
    within_frame(find(:xpath, "//iframe")[:id]) do
      if veiculo == "moto"
        elementinputvalorliberado.set("100000")
      else
        elementinputvalorliberado.set("500000")
      end
      elementinputvalorliberado.send_keys :tab

      find(:xpath, "//select[@id='cbo']").click
      sleep 2
      find(:xpath, "//select[@id='cbo']").all("option").sample.select_option
      find(:xpath, "//input[@value='36']").click
    end
  end

  def clicar_preencher_proposta
    within_frame(find(:xpath, "//iframe")[:id]) do
      elementbuttonpreencherproposta.click
    end
  end

  def preencher_cpf_cliente(cpf)
    within_frame(find(:xpath, "//iframe")[:id]) do
      elementinputpreenchercpfcliente.set(cpf)
    end
  end

  def clicar_botao_validar_cliente
    within_frame(find(:xpath, "//iframe")[:id]) do
      elementbuttonvalidarcpfclient.click
    end
  end

  def clicar_proximo_base_historica
    within_frame(find(:xpath, "//iframe")[:id]) do
      elementbuttonproximo.click
    end
  end

  # def preenche_dados_operador
  #     within_frame(find(:xpath, "//iframe")[:id]) do
  #         find("select[name='P_PROMOTOR']").all("option").sample.select_option
  #         find("select[name='P_AGENTE_LOJA']").all("option")[2].select_option
  #         find("select[name='P_AGENTE_LOJA']").all("option")[1].select_option unless find("select[name='P_AGENTE_LOJA']").value.eql?("") == false
  #     end
  # end

  #-->> {20210310} - [{DADOS ALTERADOS POR VALMIR} {EM 10/03/2021}]
  def preenche_dados_operador(moto = false)
    within_frame(find(:xpath, "//iframe")[:id]) do
      find("select[name='P_PROMOTOR']").all("option").sample.select_option
      if moto
        find("select[name='P_AGENTE_LOJA']").all("option")[1].select_option
      else
        sleep 2
        find("select[name='P_AGENTE_LOJA']").all("option")[2].select_option
      end
      find("select[name='P_AGENTE_LOJA']").all("option")[1].select_option unless find("select[name='P_AGENTE_LOJA']").value.eql?("") == false
    end
  end

  def clicar_ok_alerta
    # IMPLEMENTADO EM SETEMBRO 2020 - FECHA TELA DE ERRO EXIBIDA DUAS VEZES
    # Este código fecha uma Alerta de erro que está sendo exibida após a consulta do CEP.
    begin
      within_frame(find(:xpath, "//iframe")[:id]) do
        while find("#popup_container").visible?
          within("#popup_container", wait: 5) do
            click_button("OK")
          end
        end
      end
    rescue
    end
  end

  def clicar_botao_ok_procedencias
    elementbuttonok.click
  end

  def preenche_dados_pessoais
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@id='P_NOME_CLI']").set(gerar_nome)
      find(:xpath, "//input[@id='P_RG_CLI']").set(gerar_rg)
      find(:xpath, "//input[@name='P_RG_DT_CLI']").set("01012005")
      find(:xpath, "//input[@name='P_RG_EMISSOR_CLI']").set("SSP")
      find(:xpath, "//select[@name='P_UF_CLI']").find("option[value='SP']").select_option
      find(:xpath, "//input[@value='M']").click
      find(:xpath, "//input[@name='P_NASC_CLI']").set(gerar_numero(1, 28) + gerar_numero(1, 12) + gerar_numero(1959, 2000))
      find(:xpath, "//input[@name='P_DEPEND_CLI']").set("0")
      find(:xpath, "//select[@name='P_EST_CIVIL_CLI']").find("option[value='2']").select_option
      find(:xpath, "//input[@id='P_NATURAL_CLI']").set("Sao paulo")
      find(:xpath, "//select[@name='P_NATURAL_UF_CLI']").find("option[value='SP']").select_option

      find(:xpath, "//input[@name='P_CEP_INI_CLI']").set(gerar_cep.split("-")[0])
      find(:xpath, "//input[@name='P_CEP_FIM_CLI']").set(gerar_cep.split("-")[1])

      @new_window = window_opened_by { find(:xpath, "/html/body/form[4]/table[7]/tbody/tr[4]/td/table/tbody/tr/td[1]/input").click }
    end

    within_window @new_window do
      find(:xpath, "//input[@value='Confirmar']").click
    end

    clicar_ok_alerta

    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_NUM_CLI']").set(gerar_numero(01, 9999))
      find(:xpath, "//select[@name='P_TIPO_MORA_CLI']").find("option[value='4']").select_option
      find(:xpath, "//input[@name='P_VLR_IMOV_CLI']").set("10000000")
      find(:xpath, "//input[@name='P_TEMP_RESID_CLI']").set("01/2015")
      find(:xpath, "//input[@id='P_DDD_FONE_CLI']").set("11")
      find(:xpath, "//input[@id='P_FONE_CLI']").set("58523222")
      find(:xpath, "//select[@name='P_CATEGORIA_FONE_CLI']").find("option[value='4']").select_option
      find(:xpath, "//input[@name='P_CONTATO_FONE_CLI']").set(gerar_nome)
      find(:xpath, "//input[@id='P_DDD_CELULAR_CLI']").set("11")
      find(:xpath, "//input[@id='P_CELULAR_CLI']").set("981429956")
      find(:xpath, "//input[@name='P_EMAIL_CLI']").set("Teste@teste.com")
      find(:xpath, "//input[@name='P_NOME_MAE_CLI']").set("Mae do cliente")
    end
  end

  # Método Genérico para ação dos botões que utilizam JavaScript para o click
  # Botão Aceitar (Histórica e Ficha/Verificações)
  def clicar_aceitar(btaceitar = nil)
    sleep 3
    result = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(result)
    sleep 3
    page.execute_script("$('#{btaceitar}').click()")
    #page.execute_script("$('.botaoVerde').click()")
    validar_alert_js
  end

  def clicar_aceitar_Verif(btaceitarverif = nil)
    #page.execute_script("$('.botaoVerde').click()")
    page.execute_script("$('.botaoVerde#{btaceitarverif}').click()")
    #validar_alert_js
  end

  # Acessa nova pagina
  def nova_tela
    result = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(result)
  end

  def processar(botao, iframe = true)
    if iframe == false
      find(:xpath, "//input[@value='#{botao}']").click
      while find(:xpath, "//div[@id='waitDialog']").visible?
      end
    else
      within_frame(find(:xpath, "//iframe")[:id]) do
        find(:xpath, "//input[@value='#{botao}']").click
        while find(:xpath, "//div[@id='waitDialog']").visible?
        end
      end
    end
    validar_alert_js
  end

  # Este método não está sendo utilizado, mas é útil para quando precisarmos pegar
  # o número da proposta na ficha
  def pegar_numero_proposta
    propostageral = all("//th/font")[5].text
    numeroproposta = propostageral.gsub("PROPOSTA: ", "")
    return numeroproposta
  end

  def validar_alert_js
    # accept_confirm do
    #     click_link('Ok')
    # end
    page.driver.browser.switch_to.alert.accept
  end

  def preenche_dados_profissionais
    within_frame(find(:xpath, "//iframe")[:id]) do
      find("select[name='P_PROF_GRAU_INSTR']").find("option[value='2']").select_option
      find("select[name='P_PROF_CLASSE']").find("option[value='1']").select_option
      find("#P_BSC_PROF").set("ANALISTA DE SISTEMAS")
      find("img[src='/imagens/lupa.jpg']").click
      find("#P_PROF_EMP").set("Omni")
      # INCLUIDO POR VALMIR 11/03/2021
      # find("input[name='P_PROF_SAL']").clear
      find("input[name='P_PROF_SAL']").set("180000")
      find("#P_PROF_TEMP").set("052014")
      find("#P_PROF_FONE_DDD").set("11")
      find("#P_PROF_FONE").set("55889966")
      find(:xpath, "//input[@name='P_PROF_CEP_INI']").set(gerar_cep.split("-")[0])
      find(:xpath, "//input[@name='P_PROF_CEP_FIM']").set(gerar_cep.split("-")[1])
      @new_window = window_opened_by { find(:xpath, "/html/body/form[4]/table[9]/tbody/tr[5]/td/table/tbody/tr/td[1]/input[3]").click }
    end

    within_window @new_window do
      find(:xpath, "//input[@name='P_NUMERO']").set(gerar_numero(01, 9999))
      find(:xpath, "//input[@value='Confirmar']").click
    end
  end

  def preencher_dados_veiculo_leve
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_FL_UTIL_AGROPECUARIA1'][@value='N']").click
      find(:xpath, "//input[@name='P_PLACA1']").set("UYT4521")
      find(:xpath, "//input[@name='P_RENAVAM1']").set("35135435434")
    end
  end

  def preencher_dados_moto
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_PLACA1']").set("CBR6500")
      find(:xpath, "//input[@name='P_RENAVAM1']").set("123456789101")
    end
  end

  def preencher_dados_veiculo_pesado
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_FL_UTIL_AGROPECUARIA1'][@value='N']").click
      find(:xpath, "//input[@name='P_PLACA1']").set("UYT4521")
      find(:xpath, "//input[@name='P_RENAVAM1']").set("35135435434")
    end
  end

  # POP - VERIFICAR SE ESTES CAMPOS SÃO OS MESMOS PARA O POP
  def preencher_dados_pop
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_FL_UTIL_AGROPECUARIA1'][@value='N']").click
      find(:xpath, "//input[@name='P_PLACA1']").set("VSJ2217")
      find(:xpath, "//input[@name='P_RENAVAM1']").set("12345678915")
    end
  end

  def popup_redisparo_politica_credito
    begin
      within_frame(find(:xpath, "//iframe")[:id]) do
        while find("#popup_content").visible?
          within("#popup_content", wait: 5) do
            click_button("OK")
          end
        end
        aguardar_redisparo
      end
    rescue
    end
  end

  def aguardar_redisparo
    begin
      within_frame(find(:xpath, "//iframe")[:id]) do
        while find("#waitDialog").visible?
        end
      end
    rescue
    end
  end

  def pegar_placa_atual
    @placa_atual = ""
    within_frame(find(:xpath, "//iframe")[:id]) do
      @placa_atual = find(:xpath, "//*[@id='P_PLACA1']").text
    end
    return @placa_atual
  end

  def buscar_dados_placa
  end

  def preenche_dados_referencias
    within_frame(find(:xpath, "//iframe")[:id]) do
      sleep 2
      find("#P_REF_NOM_FAMILIA").set("contato")
      find("#P_REF_DDD_FAMILIA").set("11")
      find("#P_REF_FONE_FAMILIA").set("58162255")
    end
  end

  def preenche_proposta_de_negocios
    within_frame(find(:xpath, "//iframe")[:id]) do
      find(:xpath, "//input[@name='P_COMPRA_DIVIDA'][@value='N']").click
    end
  end

  def preencher_procedencia
    all(:xpath, "//input[@name='P_PROCEDE_RES']")[1].click
    find(:xpath, "//input[@name='P_EM_NOME_RES']").set(gerar_nome)
    find(:xpath, "//input[@name='P_CONTATO_RES']").set(gerar_numero)
    find(:xpath, "//input[@name='P_QUAL_END_RES']").set(gerar_cep)
    find(:xpath, "//input[@name='P_RELACAO_RES']").set("Teste pai")
    find(:xpath, "//textarea[@name='P_INFORMACAO_RES']").set("Teste Info")
    find(:xpath, "//input[@id='bt_grava_res']").click
  end

  def preencher_procedencia_prof
    all(:xpath, "//input[@name='P_PROCEDE_COM']")[1].click
    find(:xpath, "//input[@name='P_EM_NOME_COM']").set(gerar_nome)
    find(:xpath, "//input[@name='P_CONTATO_COM']").set(gerar_numero)
    find(:xpath, "//input[@name='P_QUAL_END_COM']").set(gerar_cep)
    find(:xpath, "//input[@name='P_RELACAO_COM']").set("Teste Ocupacao")
    find(:xpath, "//textarea[@name='P_INFORMACAO_COM']").set("Teste Info")
    find(:xpath, "//input[@id='bt_grava_com']").click
  end

  def preencher_procedencia_fam
    all(:xpath, "//input[@name='P_PROCEDE_FAM1']")[1].click
    find(:xpath, "//input[@name='P_EM_NOME_FAM1']").set("ASDFG")
    find(:xpath, "//input[@name='P_CONTATO_FAM1']").set("ASDFG")
    find(:xpath, "//input[@name='P_QUAL_END_FAM1']").set("ASDFG")
    find(:xpath, "//input[@name='P_RELACAO_FAM1']").set("ASDFG")
    find(:xpath, "//textarea[@name='P_INFORMACAO_FAM1']").set("ASDFG")
    find(:xpath, "//input[@id='bt_grava_fam1']").click
  end

  def selecionar_correspondencia
    all(:xpath, "//input[@name='P_CORRESP_CLI']")[0].click
  end

  def clicar_reprocessar_travas
    find(:xpath, "//table/tbody/tr/td/input[@id='btReprocessarTravas']").click
  end

  def clicar_aba_ficha_verificacoes
    find(:xpath, "//table/tbody/tr/td/input[@id='btnFichaVerificacao']").click
  end

  def clicar_aceitar_procedencia
    find(:xpath, "//input[@id='btnAceitar']").click
  end

  def clicar_aba_decisao
    find(:xpath, "//table/tbody/tr/td/input[@value='Decisão']").click
  end

  def desabilitar_checked
    #Método para desabilitar butões checked que darão conflito em testes futuros
    find(:xpath, "//table/tbody/tr/td/input[@value='#008_COMPROVANTE DE RENDA']").click
    find(:xpath, "//table/tbody/tr/td/input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").click
  end

  def preencher_parecer_final
    find(:xpath, "//table/tbody/tr/td/textarea[@name='p_parecer']").set("Teste Preenchimento Parecer Final")
  end

  def clicar_validar_proposta
    find(:xpath, "//table/tbody/tr/td/input[@value='Validar']").click
  end

  def selecionar_usuario
    #Usuario: 021SERGIO_SILVA, Senha: FH6JDXYT
    find(:xpath, "//table/tbody/tr/td/select/option[@value='115855']").click
    find(:xpath, "//table/tbody/tr/td/input[@name='p_senha_liberacao']").set("FH6JDXYT")
  end

  def clicar_enviar_para_mesa
    sleep 2
    comando = "[onclick='submete();']"
    page.execute_script("$(#{comando}).click()")
    #find(:xpath, "//table/tbody/tr/td/input[@value='Enviar para a Mesa']").click
    sleep 2
    validar_alert_js
    page.driver.browser.switch_to.alert.dismiss
  end
end
