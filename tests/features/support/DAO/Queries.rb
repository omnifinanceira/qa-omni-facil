class Queries < SQLConnect
 
    def pegar_cpf
        lista_cpf = select("SELECT a.cpf_cgc
            FROM clientes_proposta  a,
                 retorno_neuro_server b
            WHERE a.proposta = b.proposta
            AND pontuacao > 95
            AND TRUNC(data_execucao) BETWEEN '01-jan-2019'  
            AND Trunc(SYSDATE)")
        return lista_cpf.shuffle.first
    end

    def pegar_placa(veiculo='leve', renavam=false)
        #VEÍCULO LEVE
        #MOTO
        #VEÍCULO PESADO
        @veiculo = veiculo
        if(@veiculo.downcase.include?('leve'))
            @veiculo = 'VEÍCULO LEVE'
        elsif(@veiculo.downcase.include?('moto'))
            @veiculo = 'MOTO'
        else
            @veiculo = 'VEÍCULO PESADO'
        end

        if renavam
            renavam = 'AND gp.cod_renavam is not null'
        end

        dados_veiculo = select("SELECT gp.placa
            FROM garantias_veiculos_proposta gp,
            proposta p,
            operacoes op
            WHERE gp.proposta = p.proposta
            AND p.operacao = op.codigo
            AND op.grupo2 = '#{@veiculo}'
            AND p.emissao >= trunc(SYSDATE - 30) --ULTIMOS 30 DIAS
            AND gp.placa is not null
            AND gp.chassi is not null")

        return dados_veiculo.shuffle.first
    end
end